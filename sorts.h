
#ifndef SORTS_H
 #define SORTS_H

#include <stdbool.h>
#include <stdlib.h>

bool sort_a(const void *arr[], size_t len, int (*cmp)(const void *, const void *));
bool sort_b(const void *arr[], size_t len, int (*cmp)(const void *, const void *));
bool sort_c(const void *arr[], size_t len, int (*cmp)(const void *, const void *));
bool sort_d(const void *arr[], size_t len, int (*cmp)(const void *, const void *));
bool sort_e(const void *arr[], size_t len, int (*cmp)(const void *, const void *));



#endif
