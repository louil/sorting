
CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline
CFLAGS+=-D_BSD_SOURCE

LDLIBS+=-lm

driver: driver.o easy_sorts.o 

.PHONY: clean debug

clean:
	-rm driver.o

debug: CFLAGS+=-g
debug: driver

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: driver
