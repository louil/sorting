
#include <stdio.h>

#include "sorts.h"

int cmp(const void *a, const void *b){
	if(*(int*)a > *(int*)b){
		printf("%d > %d\n", *(int*)a, *(int*)b);
		return 1;
	}
	else if(*(int*)a < *(int*)b){
		printf("%d < %d\n", *(int*)a, *(int*)b);
		return -1;
	}
	else{
		printf("%d = %d\n", *(int*)a, *(int*)b);
		return 0;
	}
}

int main(void){
	size_t len = 8;
	const void **arr;

	arr = malloc(sizeof(arr) * len);

	int a = 6;
	int b = 8;
	int c = 1;
	int d = 4;
	int e = 5;
	int f = 3;
	int g = 7;
	int h = 2;

	arr[0] = &a;
	arr[1] = &b;
	arr[2] = &c;
	arr[3] = &d;
	arr[4] = &e;
	arr[5] = &f;
	arr[6] = &g;
	arr[7] = &h;
	
	//sort_a(arr, len, cmp);
	//sort_b(arr, len, cmp);
	//sort_c(arr, len, cmp);
	//sort_d(arr, len, cmp);
	sort_e(arr, len, cmp);
/*
	for(int i = 0; i < (signed)len; ++i){
		printf("%d\n", *(int*)arr[i]);
	}
*/
	free(arr);
}
